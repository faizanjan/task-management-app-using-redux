export const action = {
  SET_TODO: "SET_TODO",
  ADD_TODO: "ADD_TODO",
  DELETE_TODO: "DELETE_TODO",
  TOGGLE_TODO: "TOGGLE_TODO",
  TOGGLE_IMPORTANT: "TOGGLE_IMPORTANT"
};

export const setTodos = (todos) => {
  return {
    type: action.SET_TODO,
    todos
  };
};

export const addTodo = (newTodo) => {
  return {
    type: action.ADD_TODO,
    newTodo,
  };
};

export const deleteTodo = (todoId) => {
  return {
    type: action.DELETE_TODO,
    todoId,
  };
};

export const toggleTodo = (todoId) => {
  return {
    type: action.TOGGLE_TODO,
    todoId,
  };
};

export const toggleImp = (todoId) => {
    return {
      type: action.TOGGLE_IMPORTANT,
      todoId,
    };
  };
