import React from 'react';

import Schedule from './Schedule';

export const Aside = () => {
  return (
    <aside>
         <div id="profile">
            <i className="fa-regular fa-user"></i>
            <span><img src="" alt=""/></span>
        </div>

        <Schedule/>
    </aside>
  )
}

export default Aside;