import { useSelector } from "react-redux";

export const Header = ({
  showImportant,
  setShowImportant,
  showPending,
  setShowPending,
}) => {

  let taskCount = useSelector(state=>state.length);
  let impTaskCount = useSelector(state=>state.filter(task=>task.isImportant).length);
  let pendingCount = useSelector(
    (state) => state.filter((todo) => !todo.checked).length
  );

  return (
    <ul id="main-menu">
      <li
        onClick={() => setShowImportant(false)}
        style={
          !showImportant
            ? {
                color: "rgb(1, 142, 100)",
                borderBottom: "2px solid rgb(1, 142, 100)",
              }
            : {}
        }
      >
        All
        <span className="badge">({taskCount})</span>
      </li>

      <li
        onClick={() => setShowImportant(true)}
        style={
          showImportant
            ? {
                color: "rgb(1, 142, 100)",
                borderBottom: "2px solid rgb(1, 142, 100)",
              }
            : {}
        }
      >
        Important
        <span className="badge">({impTaskCount})</span>
      </li>

      <div className="show-pending-container">
        <input
          type="checkbox"
          id="showPending"
          defaultChecked={showPending}
          onClick={() => setShowPending(!showPending)}
        />
        <label htmlFor="showPending">
          Show Pending
          <span className=" badge approved">({pendingCount})</span>
        </label>
      </div>
    </ul>
  );
};

export default Header;
