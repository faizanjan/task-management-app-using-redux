import React from 'react';
import TodosContainer from './TodosContainer';

export const Main = () => {
  return (
    <main>
        <TodosContainer/>
    </main>
  )
}

export default Main
