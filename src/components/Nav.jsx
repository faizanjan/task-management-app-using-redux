
import React from 'react'

export const Nav = () => {
  return (
    <nav>
    <ul id="menu">
      <li>
        <a href="">
          <i className="fa-solid fa-inbox"></i>
          Inbox
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-regular fa-star"></i>
          Today
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-regular fa-calendar"></i>
          Upcoming
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-solid fa-hashtag"></i>
          Important
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-solid fa-handshake"></i>
          Meetings
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-solid fa-trash"></i>
          Trash
        </a>
      </li>
    </ul>

    <ul id="category">
      <li>
        <a href="">
          <i className="fa-solid fa-users"></i>
          Family
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-regular fa-sun"></i>
          Vacation
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-solid fa-arrow-trend-up"></i>
          Festival
        </a>
      </li>
      <li>
        <a href="">
          <i className="fa-solid fa-music"></i>
          Concerts
        </a>
      </li>
    </ul>
  </nav>
  )
}

export default Nav