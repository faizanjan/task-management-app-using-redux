import React from "react";

export const Schedule = () => {
  const meetings = new Array(20).fill("");
  return (
    <div id="schedule">
        <h1>Meetings</h1>
      {meetings.map((meeting, index) => (
        <div className="meeting-card" key={index}>
          <i className="fa-solid fa-ellipsis-vertical"></i>
          <div className="meeting-detail">
            <h5 className="meeting-time">08:00 - 09:00 AM</h5>
            <h2 className="meeting-topic">Product Review</h2>
          </div>
          <div className="attendies">
            <img
              src="https://pinnacle.works/wp-content/uploads/2022/06/dummy-image.jpg"
              alt=""
            />
            <img
              src="https://pinnacle.works/wp-content/uploads/2022/06/dummy-image.jpg"
              alt=""
            />
            <img
              src="https://pinnacle.works/wp-content/uploads/2022/06/dummy-image.jpg"
              alt=""
            />
            <img
              src="https://pinnacle.works/wp-content/uploads/2022/06/dummy-image.jpg"
              alt=""
            />
          </div>
        </div>
      ))}
    </div>
  );
};

export default Schedule;
