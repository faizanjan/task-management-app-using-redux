import { toggleTodo, toggleImp } from "../actions/Actions";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";

export const Todo = ({ todo, deleteTodo }) => {

  let isChecked = useSelector(
    (state) => state.find((a_todo) => a_todo.id === todo.id).checked
  );
  
  let isImportant = useSelector(
    (state) => state.find((a_todo) => a_todo.id === todo.id).isImportant
  );
  
  let dispatch = useDispatch();

  const handleClick = () => {
    axios
      .put("http://localhost:3000/todos/"+todo.id, {
        name: todo.name,
        isImportant: todo.isImportant,
        checked: !todo.checked
      })
      .then(res=>{
        dispatch(toggleTodo(todo.id));
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleImportant = () => {
    axios
      .put("http://localhost:3000/todos/"+todo.id, {
        name: todo.name,
        isImportant: !todo.isImportant,
        checked: todo.checked
      })
      .then(res=>{
        dispatch(toggleImp(todo.id));
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className="todo">
      <input
        id={todo.id}
        type="checkbox"
        defaultChecked={isChecked}
        onClick={handleClick}
      />
      <label
        htmlFor={todo.id}
        style={isChecked ? { textDecoration: "line-through" } : {}}
      >
        {todo.name}
      </label>
      <i
        className={
          (isImportant ? "fa-solid " : "fa-regular ") + "fa-star todo-star"
        }
        onClick={handleImportant}
      ></i>
      <i
        className="badge fa-solid fa-trash"
        onClick={() => deleteTodo(todo.id)}
      ></i>
    </div>
  );
};

export default Todo;
