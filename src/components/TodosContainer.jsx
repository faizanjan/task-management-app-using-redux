import { useEffect, useState } from "react";
import { setTodos, addTodo, deleteTodo } from "../actions/Actions";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";

import Todo from "./Todo";
import Header from "./Header";

export const TodosContainer = () => {
  let [newTodo, setNewTodo] = useState("");
  let [showImportant, setShowImportant] = useState(false);
  let [showPending, setShowPending] = useState(false);

  let tasks = useSelector((state) => state);
  let dispatch = useDispatch();

  useEffect(() => {
    axios
      .get("http://localhost:3000/todos")
      .then((res) => {
        dispatch(setTodos(res["data"]));
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const handleNewTodo = (e) => {
    e.preventDefault();
    if (newTodo.trim() === "") {
      alert("Field can't be empty");
      return;
    }

    let newTodoObj = {
      name: newTodo,
      checked: false,
      isImportant: false,
    };
    axios
      .post("http://localhost:3000/todos", newTodoObj)
      .then(function (response) {
        dispatch(addTodo(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
    setNewTodo("");
  };

  const handleDeleteTodo = (todoId) => {
    axios
      .delete("http://localhost:3000/todos/" + todoId)
      .catch(function (error) {
        console.log(error);
      });
    dispatch(deleteTodo(todoId));
  };

  let filteredTasks = tasks.filter(
    (todo) =>
      (!showImportant || todo.isImportant) && (!showPending || !todo.checked)
  );
  return (
    <div className="todos-container">
      <h1>Tasks</h1>
      <Header
        showImportant={showImportant}
        setShowImportant={setShowImportant}
        showPending={showPending}
        setShowPending={setShowPending}
      />
      <form id="add-todo-form" onSubmit={(e) => handleNewTodo(e)}>
        <input
          type="text"
          value={newTodo}
          onChange={(e) => setNewTodo(e.target.value)}
          placeholder="Add a new Task..."
        />
        <button type="submit">Add</button>
      </form>
      {filteredTasks.map((todo) => (
        <Todo todo={todo} key={todo.id} deleteTodo={handleDeleteTodo} />
      ))}
    </div>
  );
};

export default TodosContainer;
