import { action as ACT } from "../actions/Actions";

let initState = [];


const taskReducer = (state = initState, action) => {
  switch (action.type) {

    case ACT.SET_TODO:
      return action.todos
      
    case ACT.ADD_TODO:
      return [
        action.newTodo,
        ...state,
      ];

    case ACT.DELETE_TODO:
      return state.filter((todo) => todo.id !== action.todoId);

    case ACT.TOGGLE_TODO:
      return state.map((todo) => {
        if (todo.id === action.todoId)
          return {
            ...todo,
            checked: !todo.checked,
          };
        else return todo;
      });

    case ACT.TOGGLE_IMPORTANT:
      return state.map((todo) => {
        if (todo.id === action.todoId)
          return {
            ...todo,
            isImportant: !todo.isImportant,
          };
        else return todo;
      });

    default:
      return state;
  }
};

export default taskReducer;
